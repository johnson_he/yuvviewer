#-------------------------------------------------
#
# Project created by QtCreator 2016-09-28T13:37:51
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = yuvViewer
TEMPLATE = app


SOURCES += main.cpp\
        yuvviewer.cpp \
    yuvimageparser.cpp

HEADERS  += yuvviewer.h \
    yuvimageparser.h

FORMS    += yuvviewer.ui
