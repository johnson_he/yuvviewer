#include "yuvimageparser.h"

YuvImageParser::YuvImageParser()
{
    this->msupportedFormats << "UYVY" << "NV12";
}

int YuvImageParser::loadYuvFile(const QString &path,
                                const QString &format,
                                int width,
                                int height,
                                QImage &image)
{
    int ret = 0;
    QFile file(path);

    if(! file.exists())
    {
        qDebug("YuvImageParser::loadYuvFile: file not exist: %s.\n", path.toLatin1().constData());
        return -1;
    }

    if(! file.open(QIODevice::ReadOnly))
    {
        qDebug("YuvImageParser::loadYuvFile: could not open file: %s.\n", path.toLatin1().constData());
        return -2;
    }

    switch(this->msupportedFormats.indexOf(format))
    {
    case 0: // UYVY
        ret = this->loadUyuvFile(file, width, height, image);
        break;
    case 1: //NV12
        ret = this->loadNv12File(file, width, height, image);
        break;
    default:
        ret = -3;
        qDebug("YuvImageParser::loadYuvFile: unsupported format.\n");
    }

    file.close();

    return ret;
}

const QStringList &YuvImageParser::getSupportedFormats()
{
    return this->msupportedFormats;
}

int YuvImageParser::clipRange(int src)
{
    if(src > 255)
    {
        return 255;
    }
    else if(src < 0)
    {
        return 0;
    }
    else
    {
        return src;
    }
}

QRgb YuvImageParser::yuv2rgb(const unsigned char y,
             const unsigned char u,
             const unsigned char v)
{
    int r,g,b;
    r = y + 1.402 * (v - 128);
    g = y - 0.34414 * (u - 128) - 0.71414 * (v - 128);
    b = y + 1.772 * (u - 128);
    return qRgb(this->clipRange(r), this->clipRange(g), this->clipRange(b));
}

int YuvImageParser::loadUyuvFile(QFile &src, int width, int height, QImage &image)
{
    int pixSize = width * height;
    int srcSize = pixSize * 2;
    //int rgbSize = pixSize * 3;

    QByteArray srcData = src.read(srcSize);
    if(srcData.length() != srcSize)
    {
        qDebug("YuvImageParser::loadUyuvFile: could not read enouth data for width(%d) x height(%d).\n", width, height);
        return -1;
    }

    const unsigned char *psrcData = (const unsigned char *)srcData.constData();
    int i = 0;
    while(i < pixSize)
    {
        image.setPixel(i % width, i / width, this->yuv2rgb(psrcData[i * 2 + 1], psrcData[i * 2], psrcData[i * 2 + 2]));
        image.setPixel((i + 1) % width, (i + 1) / width, this->yuv2rgb(psrcData[i * 2 + 3], psrcData[i * 2], psrcData[i * 2 + 2]));
        i += 2;
    }

    return 0;
}

int YuvImageParser::loadNv12File(QFile &src, int width, int height, QImage &image)
{
    int pixSize = width * height;
    int srcSize = pixSize * 3 / 2;
    //int rgbSize = pixSize * 3;

    QByteArray srcData = src.read(srcSize);
    if(srcData.length() != srcSize)
    {
        qDebug("YuvImageParser::loadNv12File: could not read enouth data for width(%d) x height(%d).\n", width, height);
        return -1;
    }

    const unsigned char *psrcData = (const unsigned char *)srcData.constData();
    int x,y;
    int i = 0;
    while(i < pixSize)
    {
        x = i % width;
        y = i / width;

        //image.setPixel(x, y, this->yuv2rgb(psrcData[i], psrcData[pixSize + y / 2 * width + x], psrcData[pixSize + y / 2 * width + x + 1]));
        //image.setPixel(x + 1, y, this->yuv2rgb(psrcData[i + 1], psrcData[pixSize + y / 2 * width + x], psrcData[pixSize + y / 2 * width + x + 1]));
        image.setPixel(x, y, this->yuv2rgb(psrcData[i], psrcData[pixSize + y / 2 * width + x], psrcData[pixSize + y / 2 * width + x + 1]));
        image.setPixel(x + 1, y, this->yuv2rgb(psrcData[i + 1], psrcData[pixSize + y / 2 * width + x], psrcData[pixSize + y / 2 * width + x + 1]));

        i += 2;
    }

    return 0;
}
