#include "yuvviewer.h"
#include "ui_yuvviewer.h"
#include <qfiledialog.h>
#include <qstring.h>
#include <qpixmap.h>
#include "yuvimageparser.h"

YuvViewer::YuvViewer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::YuvViewer)
{
    ui->setupUi(this);

    YuvImageParser parser;
    ui->cbformat->addItems(parser.getSupportedFormats());

    connect(ui->pbbrowser, SIGNAL(clicked()), this, SLOT(onPushOpen()));
    connect(ui->pbshow, SIGNAL(clicked()), this, SLOT(onPushShow()));
}

YuvViewer::~YuvViewer()
{
    delete ui;
}

void YuvViewer::onPushOpen()
{
    QString filePath = QFileDialog::getOpenFileName(this, tr("Open file"), "", tr("Allfile(*.*);;yuvfile(*.yuv)"));
    if(! filePath.isNull())
    {
        ui->lepath->setText(filePath);
    }
}

void YuvViewer::onPushShow()
{
    QImage image(ui->lewidth->text().toInt(), ui->leheight->text().toInt(), QImage::Format_RGB888);
    YuvImageParser parser;

    int ret = parser.loadYuvFile(ui->lepath->text(),
                       ui->cbformat->currentText(),
                       ui->lewidth->text().toInt(),
                       ui->leheight->text().toInt(),
                       image);

    if(ret < 0)
    {
        qDebug("YuvViewer::onPushShow: failed to parse data: %d.\n", ret);
        return;
    }

    ui->limage->setPixmap(QPixmap::fromImage(image));
}
