#ifndef YUVVIEWER_H
#define YUVVIEWER_H

#include <QWidget>

namespace Ui {
class YuvViewer;
}

class YuvViewer : public QWidget
{
    Q_OBJECT

public:
    explicit YuvViewer(QWidget *parent = 0);
    ~YuvViewer();

private slots:
    void onPushOpen();
    void onPushShow();
private:
    Ui::YuvViewer *ui;
};

#endif // YUVVIEWER_H
