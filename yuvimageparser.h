#ifndef YUVIMAGE_H
#define YUVIMAGE_H

#include <QImage>
#include <QStringList>
#include <QFile>

class YuvImageParser
{
public:
    explicit YuvImageParser();

    int loadYuvFile(const QString &path,
                    const QString &format,
                    int width,
                    int height,
                    QImage &image);
    const QStringList &getSupportedFormats();
private:
    int loadUyuvFile(QFile &src, int width, int height, QImage &image);
    int loadNv12File(QFile &src, int width, int height, QImage &image);

    QRgb yuv2rgb(const unsigned char y,
                 const unsigned char u,
                 const unsigned char v);
    int clipRange(int src);
private:
    QStringList msupportedFormats;
};

#endif // YUVIMAGE_H
